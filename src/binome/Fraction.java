package binome;

public final class Fraction {

	private final int numerateur;
	private final int denominateur;

	private  static final Fraction ZERO = new Fraction(0, 1);
	private  static final Fraction UN = new Fraction(1, 1);



	public Fraction(int numerateur, int denominateur) {

		this.numerateur = numerateur;
		this.denominateur = denominateur;
	}

	public Fraction(int numerateur) {

		this.numerateur = numerateur;
		this.denominateur = 1;

	}

	public Fraction() {

		this.numerateur = 0;
		this.denominateur = 1;
	}

	public int getNumerateur() {
		return numerateur;
	}

	public int getDenominateur() {
		return denominateur;
	}

	public double getNumerateurDouble() {
		return numerateur;
	}

	public double getDenominateurDouble() {
		return denominateur;
	}

	public String getNumerateurChaine() {
		 
		return String.valueOf(numerateur);
	}

	public String getDenominateurChaine() {
		
		return String.valueOf(denominateur);
		
	}
}
